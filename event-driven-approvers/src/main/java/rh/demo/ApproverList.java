package rh.demo;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class ApproverList implements Serializable{
    private Map<String, Approver> approvers = new HashMap<>();

    public Collection<Approver> getApprovers() {
        return approvers.values();
    }

    public void add(Approver approver) {
        approvers.put(approver.getId(), approver);
    }

    public void update(Approver approver) {
        approvers.put(approver.getId(), approver);
    }
}

