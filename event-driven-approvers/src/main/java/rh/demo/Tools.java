package rh.demo;

import org.kie.api.runtime.ClassObjectFilter;
import org.kie.api.runtime.KieContext;
import org.kie.api.runtime.KieRuntime;
import org.kie.api.runtime.process.ProcessContext;
import org.kie.api.runtime.rule.FactHandle;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Tools {

    public static List<String> stringToList(String input) {
        if (input == null || input.isEmpty()) {
            return Collections.emptyList();
        }

        return Arrays.asList(input.split(","));

    }

    public static void insertApprovers(ProcessContext ctx, String variableName) {

        ApproverList list = (ApproverList) ctx.getVariable(variableName);
        list.getApprovers()
                .forEach(ctx.getKieRuntime()::insert);
    }

    public static void retractApprovers(KieContext ctx) {
        KieRuntime runtime = ctx.getKieRuntime();
        for (FactHandle handle : runtime.getFactHandles(new ClassObjectFilter(Approver.class))) {
            runtime.delete(handle);
        }
    }
}

