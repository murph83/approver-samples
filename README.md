Event Driven Approval Flow for BPM Suite 6.4
============================================

This project demonstrates the use of signal events to manage a two-tiered approval system. You can import it into Business Central by cloning this repository from the Authoring > Administration menu.

![collect-approvals](event-driven-approvers.collect-approvals.png)


The main process is collect-approvals. It accepts two comma separated lists of actor IDs as input. One denotes primary approvers, one denotes secondary approvers. This inital list of approvers is collected and used to trigger the Approval Flow event subprocess, once per approver. Each user should now see a task in their inbox. Each time a user completes the approval task, the main process will evaluate the completion rules to determine if sufficient approval has been received. The rules
are encoded in approval-rules.drl, and are as folows:


'''
If a group has only primary approvers, every primary approver has to approve the change. If a group contains primary approvers and secondary approvers, only one approval is required (either from primary/secondary).
'''

Additional approvers may be added to the process at any time by sending the appropriate signal. A sample process (add-approver) has been included, which takes in an approver id, as well as the process instance id of the running process to signal.
